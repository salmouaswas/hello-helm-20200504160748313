FROM node:12

RUN mkdir /app
WORKDIR /app

# Copy and install the package
COPY package*.json /app/
COPY tsconfig.json /app
RUN npm install --only=prod
RUN apt-get update && apt-get install -y git

# Copy over the source code
COPY server /app/server

# Copy and install shared components
COPY shared /app/shared
RUN cd shared && npm install --only=prod && cd ..


ENV NODE_ENV production
ENV PORT 3000

EXPOSE 3000

CMD ["npm", "start"]
