const { getUploadToken } = require("../");

let retrieveUser = jest.fn();
let updateUserWithToken = jest.fn();
let sendMessage = jest.fn().mockImplementation(() => {})

jest.mock('../../../shared', () => {
	return {
	  DatabaseInstance: jest.fn().mockImplementation(() => { return {
      retrieveUser,
      updateUserWithToken
    }}),
    sendMessage: jest.fn().mockImplementation(() => {})
	}
});

describe("Testing get upload token", () => {
  beforeEach(() => jest.clearAllMocks());

  it("should return an error if the retrieve user fails", async () => {
    retrieveUser = jest.fn().mockImplementation(() => {
      throw 'Error with retrieve user'
    });

    let error:any;
    try {
      await getUploadToken('12345');
    } catch (e) {
      error = e;
    }
    expect(error.status).toBe(500);

    expect(error.code).toBe('Unable to connect to db2');
  });
  it("should return an error if the update user fails", async () => {
    retrieveUser = jest.fn().mockImplementation(() => [{phoneNumber: '3267777777'}]);
    updateUserWithToken = jest.fn().mockImplementation(() => {
      throw 'Error with update user'
    });

    let error:any;
    try {
      await getUploadToken('12345');
    } catch (e) {
      error = e;
    }
    expect(error.status).toBe(500);

    expect(error.code).toBe('Unable to connect to db2 to update');
  });
  it("should return a success and a random number (6 characters) if all goes well", async () => {
    retrieveUser = jest.fn().mockImplementation(() => [{phoneNumber: '3267777777'}]);
    updateUserWithToken = jest.fn().mockImplementation(() => 'done');
    const value = await getUploadToken('12345');
    expect(value.length).toBe(6);
  });
});
