import * as crypto from "crypto";
import moment from "moment";
import CustomError from '../../shared/utils/CustomError';
import { DatabaseInstance } from "../../shared";
import { sendMessage  } from "../../shared";

function generateToken(len: number) {
  return crypto
    .randomBytes(Math.ceil(len / 2))
    .toString('hex')
    .slice(0, len)
}

module.exports = {
  /**
   * Get upload token and save the token to the user record
   */
  getUploadToken: async (uid: string) => {
    const data = generateToken(6).toLowerCase();
    const currentDate = moment().format('YYYY-MM-DD HH:mm:ss');
    let database;
    let user: any;
    try {
      database = new DatabaseInstance();
      user = await database.retrieveUser(uid);
    } catch (e) {
      throw new CustomError('Unable to connect to db2', 500);
    }

    if (!user || user.length <= 0) {
      throw new CustomError('User Not Found', 404);
    }

    const phoneNumber = `+1${user[0].PHONE_NUMBER}`
    
    try {
      database.updateUserWithToken(data, currentDate, uid);
    } catch (e) {
      throw new CustomError('Unable to connect to db2 to update', 500);
    }
      
    try {
      sendMessage(phoneNumber, data);
    } catch (e) {
      throw new CustomError('Unable to send message', 500);
    }
  
    return data;
 
  }
}
