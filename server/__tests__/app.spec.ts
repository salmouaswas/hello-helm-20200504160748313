const request = require("supertest");
const app = require("../app");
const { getUploadToken } = require("../controllers");

import CustomError from '../../shared/utils/CustomError';

jest.mock('../controllers', () => {
  return {
    getUploadToken: jest.fn(() => "123476")
  }
});
describe("Test express routes", () => {
  beforeEach(() => jest.clearAllMocks());
  test("should respond to health with status 200", async (done) => {
    const response = await request(app).get("/health");
    expect(response.statusCode).toBe(200);
    done();
  });
  test("should respond to get upload token with status 200", async (done) => {
    const response = await request(app).get("/getUploadToken/1234");
    expect(response.body).toEqual({ token: "123476" });
    expect(response.statusCode).toBe(200);
    done();
  });
  test("should respond with error status and code", async (done) => {
    getUploadToken.mockImplementation(() => {throw new CustomError('Error', 500);});
    const response = await request(app).get("/getUploadToken/1234");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Error');
    done();
  });
  test("sanity check test", async (done) => {
    getUploadToken.mockImplementation(() => {throw 'error';});
    const response = await request(app).get("/getUploadToken/1234");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Something went wrong');
    done();
  });  
});

export {};