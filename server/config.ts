import FunctionConfig from "./types/FunctionConfig";

export const config: FunctionConfig = {
  projectId: "",
  encryption: {
    defaultAlgorithm: "",
    keyPath: "encryptionKeyPath",
    defaultVersion: 1,
  },
  upload: {
    bucket: "upload-bucket", //
    recordsDir: "records",
    testsDir: "tests",
    tokenValidityPeriod: 2, // in hours
    bucketForArchive: "archive-bucket",
  },
};
