interface FunctionConfig {
  projectId: string
  encryption: {
    defaultAlgorithm: string
    keyPath: string
    defaultVersion: number
  }
  upload: {
    bucket: string
    recordsDir: string
    testsDir: string
    tokenValidityPeriod: number // in hours
    bucketForArchive: string
  }
}

export default FunctionConfig;
