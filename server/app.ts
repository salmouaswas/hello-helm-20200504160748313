const express = require('express')
const dotenv = require('dotenv');
dotenv.config();
const app = express();
const port = process.env.PORT || 3000;
const { getUploadToken } = require("./controllers");

app.get("/getUploadToken/:uuid", async (req: { params: { uuid: string; }; }, res: { status: (arg0: number) => { (): any; new(): any; send: { (arg0: { token: string; }): void; new(): any; }; }; }) => {
  try {
    const token = await getUploadToken(req.params.uuid);
    res.status(200).send({ token });
  } catch(e) {
    const errorCode = e.code || 'Something went wrong';
    const errorStatus = e.status || 500;
    console.error(`Upload Token Request Failed - User: ${req.params.uuid} - ${errorStatus} - ${errorCode}`);
    if (e.stack) console.error(e.stack);
    res.status(errorStatus).send(errorCode);
  }
});

app.use('/health', (req: any, res: { 
  status: (arg0: number) => { 
    (): any; new(): any; send: { (arg0: any): void; new(): any; }; }; 
}) => {
  res.status(200).send('Upload Token Service OK');
});


app.listen(port, function() {
   console.log('Server started on port: ' + port);
});

module.exports = app;

export {};