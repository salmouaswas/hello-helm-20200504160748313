module.exports = {
  transform: {
    '^.+\\.js$': 'babel-jest',
    '.(ts)': 'ts-jest',
  },

  testEnvironment: 'node',
  modulePathIgnorePatterns: ['node_modules', 'chart', 'build'],

  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.ts?$',

  moduleFileExtensions: ['ts', 'js', 'json', 'node'],
  collectCoverage: true,
  collectCoverageFrom: [
    'server/**/*.{js,ts}',
  ]
};
