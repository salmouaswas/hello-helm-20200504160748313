# Get Upload Token
Get upload token is used to fetch an upload token for a given user. It takes in a User's ID, which must be in the database, and creates an upload token, which is stored in the database, sent to the user via a Twilio text, and the user's device, for validation. 

## Testing in Docker
Before pushing your code, please test your code in docker to verify that it works. From the root of the project:  
 - `docker build . -t=get-upload-token` will build the docker container. It will take a minute, the db2 driver is large.  
 - `docker run -p 3000:3000 -it get-upload-token sh` will run the terminal interactivley (`-it`), and get you into a shell (`sh`) inside your docker container.  
 - Copy your .env values into the container. You'll have to `export VAR=val` for each of them.  
 - Run `npm start`. Observe any issues or warnings.  
To exit the shell, use `Ctrl-D`.


## Git - Shared Folder
The shared folder in this makes the project somewhat complicated. To updated to the latest shared modules, run the following command:  
```shell
git subtree pull --prefix=shared https://bitbucket.org/deloittedigitalcanada/shared.git master
```

To break that out a little:  
 - `--prefix=shared` - specifies the directory that we will put it in, in the parent repo  
 - `https://bitbucket.org/deloittedigitalcanada/shared.git master` -  specifies the repo and branch that we want to pull  

Similarly, if you have changes to shared in another repo, you can push to the share repo as well, using the following pattern:
```shell
git subtree push --prefix=shared https://bitbucket.org/deloittedigitalcanada/shared.git {not_master}
```