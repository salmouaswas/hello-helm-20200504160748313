export const config: FunctionConfig = {
  projectId: "AH",
  utcOffset: -5,
  encryption: {
    defaultAlgorithm: "aes-256-gcm",
    keyPath: "ENCRYPTION_KEY",
    defaultVersion: 1,
  }
};
