/**
 * Convert timestamp (expressed in seconds since the Epoch) to "YYYY-MM-DD HH:mm:ss" format
 * @param timestamp
 */
import * as moment from "moment";

import { config } from "../config";

const TIMESTAMP_FORMAT = "YYYY-MM-DD HH:mm:ss";

function formatTimestamp(timestamp: number) {
  return moment.unix(timestamp).utcOffset(config.utcOffset).format(TIMESTAMP_FORMAT);
}

export default formatTimestamp;