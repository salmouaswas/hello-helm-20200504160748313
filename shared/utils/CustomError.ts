export default class CustomError extends Error {
    constructor(code: string = 'GENERIC', status: number = 500, ...params: any) {
        super(...params)

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, CustomError)
        }

        this.code = code
        this.status = status
    }

    code: string;
    status: number;
}