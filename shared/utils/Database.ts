import ibmdb from "ibm_db";

const { DATABASE, DB_HOST, DB_USER, DB_PWD, DB_PORT, DB_SCHEMA } = process.env;
const connectionConfig = `DATABASE=${DATABASE};HOSTNAME=${DB_HOST ? DB_HOST.split(',')[0] : ''};CURRENTSCHEMA=${DB_SCHEMA};UID=${DB_USER};PWD=${DB_PWD};PORT=${DB_PORT}`;
const alternateConnectionConfig = `DATABASE=${DATABASE};HOSTNAME=${DB_HOST ? DB_HOST.split(',')[1] : ''};CURRENTSCHEMA=${DB_SCHEMA};UID=${DB_USER};PWD=${DB_PWD};PORT=${DB_PORT}`;

class DatabaseInstance {
  connection: any

  establishConnection() {
    return new Promise((resolve, reject) => {
      // @ts-ignore
      ibmdb.open(connectionConfig, (err: any, conn: any) => {
        if (err) {
          console.error('Error connecting to database once', err);
          // validate that this is a communication error sqlcode, if so, try again with main database
          if (err.sqlcode === -30081 || err.sqlcode === -30080 || err.sqlcode === -4499) {
            // @ts-ignore
            ibmdb.open(connectionConfig, (err: any, conn: any) => {
              if (err) {
                console.error('Error connecting to database twice', err);
                // validate that this is a communication error sqlcode, if so, try with recovery database
                if (err.sqlcode === -30081 || err.sqlcode === -30080 || err.sqlcode === -4499) {
                  // @ts-ignore
                  ibmdb.open(alternateConnectionConfig, (err: any, conn: any) => {
                    if (err) {
                      console.error('Error connecting to second database', err);
                      reject()
                    } else {
                      this.connection = conn;
                      resolve();
                    }
                  })
                } else {
                  reject();
                }
              } else {
                this.connection = conn;
                resolve();
              }
            })
          } else {
            reject()
          }
        } else {
          this.connection = conn;
          resolve();
        }
      })
    })
  }

  beginTransactionSync() {
    this.connection.beginTransactionSync();
  }

  rollbackTransactionSync() {
    this.connection.rollbackTransactionSync();
  }

  commitTransactionSync() {
    this.connection.commitTransactionSync();
  }

  closeConnection() {
    this.connection.closeSync();
  }

  async updateUserWithToken(uploadKey: string, timestamp: any, uid: string ) {
    await this.establishConnection();
    return new Promise((resolve, reject) => {
      const queryString = "UPDATE USERS SET UPLOAD_KEY = (?), UPLOAD_KEY_TIMESTAMP = (?) WHERE USER_ID = (?)";
      const bindParams = [`${uploadKey}`, `${timestamp}`, uid];
      this.connection.query(queryString,bindParams,
        (err: any, data: any) => {
          if (err) {
            console.error('update user', err);
            this.connection.closeSync();
            reject(err);
          } else {
            this.connection.closeSync();
            resolve(data);
          }
        }
      )
    })
  }

  async putContactData(contactData: ContactData) {
    return new Promise((resolve, reject) => {
      const queryString = `INSERT INTO CONTACT_DATA (
        CENTRAL_USER_ID,
        PERIPHERAL_USER_ID,
        RECORD_TIMESTAMP,
        UNIX_MS_EPOCH,
        RSSI,
        TX_POWER,
        PROTOCOL_VERSION,
        MODEL_C,
        MODEL_P,
        ORG_CODE
        ) VALUES (?,?,?,?,?,?,?,?,?,?)`;

      const bindParams = [
        contactData.centralUser,
        contactData.peripheralUser,
       `${contactData.timestamp}`,
        contactData.unixTimestamp,
        contactData.rssi,
        contactData.txPower,
        contactData.protocolVersion,
        `${contactData.modelC}`,
        `${contactData.modelP}`,
        `${contactData.org}`,
      ];
 
      this.connection.query(queryString,bindParams,
        (err: any, data: any) => {
          if (err) {
            console.error('insert contact data', err);
            reject(err);
          } else {
            resolve(data);
          }
        }
      )
    })
  }

  async retrieveUser(uid: string) {
    await this.establishConnection();
    return new Promise((resolve, reject) => {
      const queryString = "SELECT USER_ID, PHONE_NUMBER, UPLOAD_KEY, UPLOAD_KEY_TIMESTAMP FROM USERS WHERE USER_ID = (?)";
      const bindParams = [uid];
      this.connection.query(queryString, bindParams,
        (err: any, data: any) => {
          if (err) {
            console.error('fetch user error', err);
            this.connection.closeSync();
            reject(err);
          } else {
            this.connection.closeSync();
            resolve(data);
          }
        }
      )
    })
  }


  async deleteData() {
    await this.establishConnection();
    return new Promise((resolve, reject) => {
      const queryString = "DELETE FROM CONTACT_DATA WHERE UPLOAD_TIMESTAMP < (CURRENT TIMESTAMP - 21 DAYS)";
      this.connection.query(queryString,
        (err: any, data: any) => {
          if (err) {
            console.error('delete error', err);
            this.connection.closeSync();
            reject(err);
          } else {
            this.connection.closeSync();
            resolve(data);
          }
        }
      )
    })
  }
}

export default DatabaseInstance;