export function isUploadData(object: any): object is UploadData {
    if (typeof object.token != 'string') {
        return false;
    }
    if (Array.isArray(object.records)) {
        let areAllRecords: boolean = true;
        object.records.every((element: any) => {
            if (!isContactRecord(element)) {
                areAllRecords = false;
                return;
            }
        });
        if (!areAllRecords) return false;
    } else {
        return false;
    }
    return true;
}

export function isContactEvent(object: any): object is ContactEvent {
    if (object.msg != "Scanning started" && object.msg != "Scanning stopped")
    {
        return false;
    }
    if (typeof object.timestamp != 'number') {
        return false;
    }
    return true;
}

export function isContactRecord(object: any): object is ContactRecord {
    if (!object.modelP || typeof object.modelP != 'string') {
        return false;
    }
    if (!object.org || typeof object.org != 'string') {
        return false;
    }
    if (!object.modelC || typeof object.modelC != 'string') {
        return false;
    }
    if (!object.rssi || typeof object.rssi != 'number') {
        return false;
    }
    if (object.txPower && typeof object.txPower != 'number') {
        return false;
    }
    if (object.msg && typeof object.msg != 'string') {
        return false;
    }
    if (!object.v || typeof object.v != 'number') {
        return false;
    }
    if (!object.timestamp || typeof object.timestamp != 'number') {
        return false;
    }        
    return true;
}