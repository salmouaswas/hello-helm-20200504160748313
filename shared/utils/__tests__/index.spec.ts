import formatTimestamp from '../formatTimestamp';
const currentDate = 1587004770;

describe("Testing format timestamp", () => {
  it("should return the correct timestamp", async () => {
    const value = await formatTimestamp(currentDate);
    expect(value).toBe('2020-04-15 21:39:30');
  });
});

export {};