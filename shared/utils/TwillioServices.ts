const { TWILLIO_TOKEN, ACCOUNT_SID, DEFAULT_NUMBER } = process.env;
const client = require('twilio')(ACCOUNT_SID, TWILLIO_TOKEN);

async function sendMessage(phoneNumber: string, token: string) {
  const body = `To upload your data to Alberta Health Services, please enter this code into the app: ${token}`
  const messageObject = {
    body,
    messagingServiceSid: DEFAULT_NUMBER,
    to: phoneNumber
  }
  client.messages
  .create(messageObject)
  .then((message: any) => console.log("SMS message sent", message.sid))
  .catch((error: any) => {
    if (error.code === 20429) {
      console.error("SMS message error, trying again", error)

      // try to send a message again before returning an error
      client.messages
      .create(messageObject)
      .then((message: any) => console.log("SMS message sent", message.sid))
      .catch((error: any) => console.error("SMS message second error", error));
    } else {
      console.error("SMS message error", error)
    }
  });
}

export default sendMessage;