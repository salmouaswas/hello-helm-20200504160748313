import { config } from "../config";

const SECRET_KEY = `PROJECTS_${config.projectId}_SECRETS_${config.encryption.keyPath}`;

const getEncryptionKey = async (): Promise<Buffer> => getEncryptionSecret(SECRET_KEY);

async function getEncryptionSecret(keyPathIncludingVersion: string): Promise<Buffer> {
  // @ts-ignore
  return Buffer.from(process.env[keyPathIncludingVersion].toString(), 'base64');
}

export default getEncryptionKey;
