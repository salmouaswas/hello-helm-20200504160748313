interface FunctionConfig {
  projectId: string
  utcOffset: number | string
  encryption: {
    defaultAlgorithm: string
    keyPath: string
    defaultVersion: number
  },
  upload?: {
    tokenExpiry?: number // in minutes
  },  
}