interface ContactRecord {
  id: number,
  modelP: string,
  org: string,
  modelC: string,
  rssi: number,
  txPower?: number,
  msg?: string,
  v: number,
  timestamp: number
}

interface ContactEvent {
  id: number,
  msg: "Scanning started" | "Scanning stopped",
  timestamp: number
}

interface UploadData {
  token: string,
  user: string | undefined,
  records: Array<ContactRecord>,
  events: Array<ContactEvent>
}

interface ServiceCredentials {
  apiKeyId: string,
  serviceInstanceId: string,
  region: string,
  endpoint: any
}

interface S3Configuration {
  bucketName: string,
  serviceCredential: ServiceCredentials
}

interface ContactData {
  centralUser: number | string,
  peripheralUser: number | string,
  timestamp: string,
  unixTimestamp: number,
  rssi: number,
  txPower?: number | null,
  protocolVersion: number,
  modelC: string,
  modelP: string,
  org: string
}